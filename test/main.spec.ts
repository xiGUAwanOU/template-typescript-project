import { plus } from '../src/main';

describe('plus', () => {
  it('should calculate correctly', () => {
    expect(plus(3, 4)).toEqual(7);
  });
});
